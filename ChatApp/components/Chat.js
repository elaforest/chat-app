import React from 'react';
import { StyleSheet, Text, TextInput, Button, View, SafeAreaView, FlatList } from 'react-native';
import {MessageItem} from './'




export class Chat extends React.Component {

    state= {
        newMessage: "",
        messages : [
            {_id:1, author:"Bob", content: "Hello Michel!", created_at: new Date()},
            {_id:2, author:"Michel", content: "Hello Bob!", created_at: new Date()},
            {_id:3, author:"Bob", content: "JEEE SUISSSSSS BOBOBOBOBOBOBOBOOBOBBO !!!!!", created_at: new Date()},
        ]
    }

    handleSubmit = e => {
        const { user } = this.props;
        const { messages, newMessage }= this.state;

        this.setState({
            messages: [
                ...messages,
                {_id: Math.random(), author: user, content: newMessage, created_at: new Date()}
            ],
            newMessage: ""
        })
    }

    render() {
        const {user} = this.props;
        const {messages, newMessage} = this.state;

        return (
            <SafeAreaView style={styles.root}>
                <FlatList 
                    data={messages}
                    renderItem= {({ item}) =>
                        <MessageItem user= {user} message={item} />
                                            }
                    keyExtractor={item => item._id}
                    style={styles.messageList}
                />
                <View style={styles.messageComposer}>
                    <TextInput style={styles.messageInput} value={newMessage} onChangeText={newMessage => this.setState({newMessage})}/>
                    <Button 
                        title="Send"
                        onPress={this.handleSubmit}
                        style={styles.sendBtn}
                        disabled={!newMessage}
                        />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
    },
    messageList:{
        flex:1,
    },
    messageComposer:{
        flex:0,
        flexDirection: "row",
        padding: 4,
        borderTopWidth: 1,
        borderColor: "#CCC",
        backgroundColor: "#EEE"
    },
    messageInput: {
        flex:1,
        backgroundColor: "white",
        paddingHorizontal: 4
    },
    sendBtn: {
        flex: 0,
    },
});
