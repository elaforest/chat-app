import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';

export class Home extends React.Component {

    state = {
        user: '',
        room: ''
    }

    render() {
        const { user, room } = this.state;
        return (
            <View style={styles.root}>
                <Text style={styles.h1}>HOME</Text>
                <Text style={styles.label}>Username</Text>
                <TextInput style={styles.input}
                    value={user}
                    //placeholder="Leave blank for anonymous"
                    onChangeText={user => this.setState({ user })}
                />
                <Text style={styles.label}>Salon</Text>
                <TextInput style={styles.input}
                    value={room}
                    //placeholder="Leave blank for anonymous"
                    onChangeText={room => this.setState({ room })}
                />
                <Button
                    title="Let's Chat !"
                    onPress={() => Actions.chat({ user, title: `Room: ${room}` } )}
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    h1: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: 'blue',
    },
    input:{
        borderWidth: 1,
        minWidth: 200,
        borderColor: 'black',
        padding:8,
        marginBottom: 15
    }
});
